# Contributing to this repo

:+1::tada: First off, thanks for taking the time to contribute! :tada::+1:

The following is a set of guidelines for contributing. These are mostly guidelines, not rules. Use your best judgement, 
and feel free to propose changes to this document in a pull request.

## Open an issue

For bugs, issues, or other discussion, please log a new issue in the repository.

This repo supports [markdown](https://docs.gitlab.com/ee/user/markdown.html), so when filing bugs make sure you check the formatting before clicking submit.

## Contributing code and content
We welcome all forms of contributions from the community. Please read the following guidelines to maximise the chances of your PR being merged.

### Communication

 - Before starting work on a feature, check if there isn't already an existing version in the parallel repos that could be referenced.
   If not, then please open an issue describing the proposed feature. We want to make sure any feature work goes smoothly. 
   We're happy to work with you to determine if it fits the current project direction and make sure no one else is already working on it.

## Code of Conduct

To ensure an inclusive community, contributors and users should follow the [code of conduct](./CODE_OF_CONDUCT.md).

### Contributor License Agreement
*We want to keep this information free, lean, w/o administrative hurdles, and as widely usable as possible.*

By contributing to this repository you grant us a non-exclusive,
irrevocable, worldwide, royalty-free, sublicenseable, transferable
license under all of Your relevant intellectual property rights
(including copyright, patent, and any other rights), to use, copy,
prepare derivative works of, distribute and publicly perform and
display the Contributions on any licensing terms, including without limitation:
(a) open source licenses like the Apache license; and (b) binary,
proprietary, or commercial licenses. Except for the licenses granted herein,
You reserve all right, title, and interest in and to the Contribution.

You confirm that you are able to grant us these rights. You represent
that You are legally entitled to grant the above license. If Your employer
has rights to intellectual property that You create, You represent that
You have received permission to make the Contributions on behalf of that
employer, or that Your employer has waived such rights for the Contributions.

You represent that the Contributions are Your original works of
authorship, and to Your knowledge, no other person claims, or
has the right to claim, any right in any invention or patent
related to the Contributions. You also represent that You are
not legally obligated, whether by entering into an agreement
or otherwise, in any way that conflicts with the terms of this license.

We acknowledge that, except as explicitly described in this
Agreement, any Contribution which you provide is on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING, WITHOUT LIMITATION,
ANY WARRANTIES OR CONDITIONS OF TITLE, NON-INFRINGEMENT,
MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE.
